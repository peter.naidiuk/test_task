import unittest
from main import get_longest_string


class TestGetLongestUniqueString(unittest.TestCase):

    def test_unique_string_at_the_beginning(self):
        input_string = "abcaaaa"
        expect_result = 3
        self.assertEqual(expect_result, get_longest_string(input_string))

    def test_unique_string_at_the_end(self):
        input_string = "aaaabc"
        expect_result = 3
        self.assertEqual(expect_result, get_longest_string(input_string))

    def test_unique_string_in_middle(self):
        input_string = "aaabcaa"
        expect_result = 3
        self.assertEqual(expect_result, get_longest_string(input_string))

    def test_unique_strings_same_length(self):
        input_string = "aawertaasdfg"
        expect_result = 5
        self.assertEqual(expect_result, get_longest_string(input_string))

    def test_unique_string_with_len_1(self):
        input_string = "aaaaaaaaaaaa"
        expect_result = 1
        self.assertEqual(expect_result, get_longest_string(input_string))
