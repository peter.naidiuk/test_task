import argparse


def _get_longest_unique_string(string, start, finish):
    index = start
    pointer = index + 1
    while index < finish:
        while pointer < finish:
            if string[index] == string[pointer]:
                sub_str1 = _get_longest_unique_string(string, index, pointer)
                sub_str2 = _get_longest_unique_string(string, pointer, finish)
                if sub_str1 > sub_str2:
                    return sub_str1
                return sub_str2
            pointer += 1
        index += 1
        pointer = index + 1
    return finish - start


def get_longest_unique_string(string):
    return _get_longest_unique_string(string, 0, len(string))


if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser(description="")
    arg_parser.add_argument("string", type=str)
    args = arg_parser.parse_args()
    print(get_longest_unique_string(args.string))

